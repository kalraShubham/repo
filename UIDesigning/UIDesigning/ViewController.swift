//
//  ViewController.swift
//  UIDesigning
//
//  Created by Shubham on 1/16/19.
//  Copyright © 2019 Guesthouser. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var dataArray:[Int]  = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
   
}


extension ViewController:UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var tableViewCell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if tableViewCell == nil
        {
            tableViewCell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        return tableViewCell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20;
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row % 2 == 0 {
            return 197
        }
        else {
            return 197 - 73
        }
    }
}

