//
//  ManageListingCell.swift
//  UIDesigning
//
//  Created by Shubham on 1/16/19.
//  Copyright © 2019 Guesthouser. All rights reserved.
//

import UIKit

class ManageListingCell: UITableViewCell {
    
    
    @IBOutlet weak var imgView:UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var updatedOnLbl: UILabel!
    @IBOutlet weak var propertyStatusView: UIView!
    @IBOutlet weak var propertyStatusLbl: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
